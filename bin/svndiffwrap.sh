#!/bin/sh

/usr/bin/vimdiff ${6} ${7}

# if vimdiff returned 1, return Subversion's fatal error code
# this lets :cq abort in the middle of a multi-file diff
if [ $? -eq 1 ]; then
   exit 2;
fi
exit 0

