if has('vim_starting')
   set encoding=utf-8
   scriptencoding=utf-8
endif

let g:mapleader=','
let g:maplocalleader=','

syntax on                       " Turn on syntax highlighting
filetype plugin on              " Enable filetype plugins
filetype indent on              " Let filetype plugins indent for me

nmap <silent> <leader>ev :e  $MYVIMRC<cr>
nmap <silent> <leader>sv :so $MYVIMRC<cr>
au! BufWritePost $MYVIMRC source $MYVIMRC


function! BuildYCM(info)
  " info is a dictionary with 3 fields
  " - name:   name of the plugin
  " - status: 'installed', 'updated', or 'unchanged'
  " - force:  set on PlugInstall! or PlugUpdate!
  if a:info.status == 'installed' || a:info.status == 'updated' || a:info.force
    let l:cmd = './install.py'
    let l:cmd = ' --clang-completer'

    if executable('go')
      let l:cmd .= ' --go-completer'
    endif
    if executable('rustc') && executable('cargo')
      let l:cmd .= ' --rust-completer'
    endif
    " Those two are not very nice yet
"    if executable('xbuild') || executable('msbuild')
"      let l:cmd .= ' --omnisharp-completer'
"    endif
    if executable('npm') && executable('tern')
      let l:cmd .= ' --tern-completer'
    endif
    " FIXME: Make it return the success/failure of an installation
    "execute BrewWrap(l:cmd)
    execute "!" . l:cmd
  endif
endfunction


" Manage plugins (use single quotes)
let g:plug_window='new'
call plug#begin('~/.vim/plugged')

" Dependencies:
" cmake python-dev python3-dev
if (version == 704 && has('patch1578')) || (version > 704) || (has('nvim'))
   if executable('cmake') && executable('python') && executable('make') && executable('cc') && executable('c++')
      Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer' }
   endif
endif
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_collect_identifiers_from_tags_files = 1
"let g:ycm_global_ycm_extra_conf = '~/.vim/ycm_extra_conf_global.py'
let g:ycm_confirm_extra_conf = 0
let g:ycm_python_binary_path = 'python3'


Plug 'w0rp/ale'
let g:ale_linters = {
         \   'c': ['clangtidy', 'cppcheck'],
         \   'cpp': ['clangtidy', 'cppcheck'],
         \}
if has('mac')
   "clang-tidy brew installed path on mac
   let g:ale_c_clangtidy_executable = '/usr/local/opt/llvm/bin/clang-tidy'
else
   let g:ale_c_clangtidy_executable = 'clang-tidy-7'
   let g:ale_c_clangformat_executable = 'clang-format-7'
endif

let g:ale_fixers = {
         \   'c': ['clang-format'],
         \   'cpp': ['clang-format'],
         \}
let g:ale_sign_column_always = 1
let g:ale_vim_vint_show_style_issues = 1

Plug 'kana/vim-altr'

Plug 'majutsushi/tagbar'
Plug 'ludovicchabant/vim-gutentags'
let g:gutentags_ctags_extra_args = ['--recurse', '--python-kinds=-i', '--c-kinds=+pl', '--c++-kinds=+pl', '--fields=+iaS', '--extra=+q']
set statusline+=%{gutentags#statusline()}
set tags=./tags;/

Plug 'justinmk/vim-dirvish'     "netwr is trash

Plug 'Shougo/denite.nvim'
Plug 'ntpeters/vim-better-whitespace'
Plug 'junegunn/vim-easy-align'

Plug 'Lokaltog/vim-easymotion'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

"Plug 'ten0s/syntaxerl', { 'for' : 'erlang' }
"Plug 'jimenezrick/vimerl', { 'for' : 'erlang' }
"Plug 'edkolev/erlang-motions.vim', { 'for' : 'erlang' }
"Plug 'vim-erlang/vim-erlang-compiler', { 'for' : 'erlang' }
"Plug 'vim-erlang/vim-erlang-omnicomplete', { 'for' : 'erlang' }

"Plug 'jmcantrell/vim-virtualenv', { 'for' : 'python' }

Plug 'autozimu/LanguageClient-neovim', { 'branch': 'next', 'do': 'bash install.sh' }
let g:LanguageClient_serverCommands = {
	\ 'c': ['cquery', '--log-file=/tmp/cq.log', '--init={"cacheDirectory":"/tmp/cquery/"}'],
	\ 'cpp': ['cquery', '--log-file=/tmp/cq.log', '--init={"cacheDirectory":"/tmp/cquery/"}'],
   \ }
let g:LanguageClient_autoStart = 1

let g:LanguageClient_loggingFile = '/tmp/LanguageClient.log'
let g:LanguageClient_loggingLevel = 'INFO'
let g:LanguageClient_serverStderr = '/tmp/LanguageServer.log'

nnoremap <f5> :call LanguageClient_contextMenu()<CR>


"if has('nvim')
"  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"else
"  Plug 'Shougo/deoplete.nvim'
"  Plug 'roxma/nvim-yarp'
"  Plug 'roxma/vim-hug-neovim-rpc'
"endif
let g:deoplete#enable_at_startup = 1

"Plug 'Shougo/neosnippet.vim'
"Plug 'Shougo/neosnippet-snippets'
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)


"Plug 'kien/ctrlp.vim'
Plug 'ctrlpvim/ctrlp.vim'

"Plug 'ctrlp-py-matcher'
"let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }


"Plug 'altercation/vim-colors-solarized'
Plug 'ciaranm/inkpot'
Plug 'sheerun/vim-wombat-scheme'    " needs xterm-256color/screen-256color

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_theme='base16'

call plug#end()

"call deoplete#custom#option('sources', {
"   \ '_': ['buffer'],
"   \ 'c': ['LanguageClient'],
"   \ 'cpp': ['LanguageClient'],
"   \})
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

autocmd BufEnter * sign define dummy
autocmd BufEnter * execute 'sign place 9999 line=1 name=dummy buffer=' . bufnr('')

set background=dark
if has('gui_running')
   colorscheme solarized

   set guifont=Powerline\ Consolas:h11,Inconsolata\ for\ Powerline:h11
   set lines=55 columns=100

   set guioptions+=a   " autoselect
   set guioptions+=c   " use console dialogs
else
   "colorscheme wombat
   colorscheme elflord
endif


set shortmess+=a                        " Use [+] [RO] [w] for modified, read-only, modified
set showcmd                             " Display what command is waiting for an operator
set ruler                               " Show pos below the win if there's no status line
set laststatus=2                        " Always show statusline, even if only 1 window
set report=0                            " Notify me whenever any lines have changed
set confirm                             " Y-N-C prompt if closing with unsaved changes
set vb t_vb=                            " Disable visual bell

set listchars=tab:→\ ,eol:$,trail:•,precedes:<,extends:>
"set lazyredraw                         " Don't repaint when scripts are running
set number                              " Show line numbering
set numberwidth=1                       " Use 1 col + 1 space for numbers
" set title
set titlestring=%F

set nobackup nowritebackup noswapfile
set noautowrite noautowriteall noautoread
set ffs=unix,dos,mac
set hidden                              " Allow unchanged buffers to hide
set scrolloff=3                         " Keep 3 lines below and above the cursor
set backspace=indent,eol,start          " Backspace over anything! (Super backspace!)
set showmatch                           " Briefly jump to the previous matching paren
set matchtime=2                         " For .2 seconds
set textwidth=78
set autoread                            " automatically re-read changes

set formatoptions=croqnj   " see :help fo-table

"set autochdir
autocmd BufEnter * silent! lcd %:p:h


""" Searching
set ignorecase                          " Default to using case insensitive searches
set smartcase                           " unless uppercase is used in regex.
set hlsearch                            " Highlight matches to the search
set incsearch                           " Incrementally search
" Clear highlights
nmap <silent> <leader>/ :nohlsearch<cr>

set mouse=a             " Note: Hold Shift to copy/paste from external


" Indenting
set autoindent
set expandtab           " Use spaces instead of tabs for autoindent/tab key
set smarttab
set tabstop=3           " tabs look like 3 spaces
set shiftwidth=3        " use 3 spaces when indenting (i.e. << or >>)
set softtabstop=3


if (exists('+colorcolumn'))
   set colorcolumn=80
   highlight ColorColumn ctermbg=9
endif

if has('folding')
   set foldenable
   set foldmethod=indent
   set foldlevel=99
   set foldlevelstart=99
   set foldtext=FoldText()
endif

" Improved Vim fold-text
" See: http://gregsexton.org/2011/03/27/improving-the-text-displayed-in-a-vim-fold.html
function! FoldText()
   " Get first non-blank line
   let l:fs = v:foldstart
   while getline(l:fs) =~? '^\s*$' | let l:fs = nextnonblank(l:fs + 1)
   endwhile
   if l:fs > v:foldend
      let l:line = getline(v:foldstart)
   else
      let l:line = substitute(getline(l:fs), '\t', repeat(' ', &tabstop), 'g')
   endif

   let l:w = winwidth(0) - &foldcolumn - (&number ? 8 : 0)
   let l:foldSize = 1 + v:foldend - v:foldstart
   let l:foldSizeStr = ' ' . l:foldSize . ' lines '
   let l:foldLevelStr = repeat('+--', v:foldlevel)
   let l:lineCount = line('$')
   let l:foldPercentage = printf('[%.1f', (l:foldSize*1.0)/l:lineCount*100) . '%] '
   let l:expansionString = repeat('.', l:w - strwidth(l:foldSizeStr.line.foldLevelStr.foldPercentage))
   return l:line . l:expansionString . l:foldSizeStr . l:foldPercentage . l:foldLevelStr
endfunction


"""" Command Line
set wildmenu                            " Autocomplete features in the status bar
set wildmode=list:longest
set wildignore+=*.o,*.obj,*.bak,*.exe,*.pyc,*.swp   " we don't want to edit these type of files

set completeopt=menuone

" allow command line editing like emacs
cnoremap <C-A>      <Home>
cnoremap <C-B>      <Left>
cnoremap <C-E>      <End>
cnoremap <C-F>      <Right>
cnoremap <C-N>      <End>
cnoremap <C-P>      <Up>
cnoremap <ESC>b     <S-Left>
cnoremap <ESC><C-B> <S-Left>
cnoremap <ESC>f     <S-Right>
cnoremap <ESC><C-F> <S-Right>
cnoremap <ESC><C-H> <C-W>

"""" Key Mappings
" work more logically with wrapped lines
noremap j gj
noremap k gk
nmap <leader>w :set invwrap<cr>


nnoremap ; :
imap jj <esc>
nmap <leader>p :set invpaste paste?<cr>
nmap <leader>rr :1,$retab<rc>

cmap w!! %!sudo tee > /dev/null %

" bind ctrl+space for omnicompletion
inoremap <Nul> <C-x><C-o>
imap <c-space> <c-x><c-o>
" allow arrow keys when code completion window is up
inoremap <Down> <C-R>=pumvisible() ? "\<lt>C-N>" : "\<lt>Down>"<CR>

" Easy align interactive
vnoremap <silent> <Enter> :EasyAlign<cr>

" * to search the word in all files in dir
"nmap * :Ag <c-r>=expand("<cword>")<cr><cr>
" seach in files
"nnoremap <space>/ :Ag

""" Open/Close/Clear quickfix window
nmap <leader>c :copen<CR>
nmap <leader>cc :cclose<CR>
nmap <leader>cq :call setqflist([])<CR>

" Toggle spell checking
map <leader>ss :setlocal spell!<cr>

nnoremap <leader>l :call ToggleRelativeAbsoluteNumber()<CR>
" Vertical split with synchronized scrolling
nmap <leader>sb :call SplitScroll()<CR>

" Ctrl + Arrows - Move around quickly
nnoremap <c-up>     {
nnoremap <c-down>   }
nnoremap <c-right>  El
nnoremap <c-left>   Bh

" Shift + Arrows - Visual select
nnoremap <s-up>     Vk
nnoremap <s-down>   Vj
nnoremap <s-right>  vl
nnoremap <s-left>   vh

if &diff
   " easily handle diffing
   vnoremap < :diffget<CR>
   vnoremap > :diffput<CR>
else
   " visual shifting (builtin-repeat)
   vnoremap < <gv
   vnoremap > >gv
endif

" <space> toggles folds opened and closed
nnoremap <space> za
" <space> in visual mode creates a fold over the marked range
vnoremap <space> zf

" Delete Empty Lines
map <leader>del :g/^\s*$/d<CR>
" Delete Double Quoted Lines
map <leader>ddql :%s/^>\s*>.*//g<CR>
" Delete Dot Runs
map <leader>ddr :s/\.\+\s*/. /g<CR>
" Delete Space Runs
map <leader>dsr :s/\s\s\+/ /g<CR>

" Reindent everything
noremap <leader>= gg=G
" Reformat paragraph
noremap <leader>gp gqap
" Reformat everything
noremap <leader>gq gggqG
" Select everything
noremap <leader>gg ggVG

" Allow setting window title for screen
if &term =~ '^screen'
   set t_ts=k
   set t_fs=\
endif

" Vertical split current file with syncronized scrolling
function! SplitScroll()
   :wincmd v
   :wincmd w
   execute "normal! \<C-d>"
   :set scrollbind
   :wincmd w
   :set scrollbind
endfunction

function! ToggleRelativeAbsoluteNumber()
   if &number
      set relativenumber
   else
      set number
   endif
endfunction

autocmd BufEnter * EnableStripWhitespaceOnSave


function! UpdateCopyrightYear()
    " Save the current search and cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")

   "http://vim.wikia.com/wiki/Automatically_Update_Copyright_Notice_in_Files
   exe '%s:'.
            \       '\cCOPYRIGHT\s*\%((c)\|©\|&copy;\)\?\s*'.
            \         '\%([0-9]\{4}\(-[0-9]\{4\}\)\?,\s*\)*\zs'.
            \         '\('.
            \           '\%('.strftime("%Y").'\)\@![0-9]\{4\}'.
            \           '\%(-'.strftime("%Y").'\)\@!\%(-[0-9]\{4\}\)\?'.
            \         '\&'.
            \           '\%([0-9]\{4\}-\)\?'.
            \           '\%('.(strftime("%Y")-1).'\)\@!'.
            \           '\%([0-9]\)\{4\}'.
            \         '\)'.
            \         '\ze\%(\%([0-9]\{4\}\)\@!.\)*$:'.
            \       '&, '.strftime("%Y").':e' |
            \ exe '%s:'.
            \       '\cCOPYRIGHT\s*\%((c)\|©\|&copy;\)\?\s*'.
            \         '\%([0-9]\{4}\%(-[0-9]\{4\}\)\?,\s*\)*\zs'.
            \           '\%('.strftime("%Y").'\)\@!\([0-9]\{4\}\)'.
            \           '\%(-'.strftime("%Y").'\)\@!\%(-[0-9]\{4\}\)\?'.
            \         '\ze\%(\%([0-9]\{4\}\)\@!.\)*$:'.
            \       '\1, '.strftime("%Y").':e'

    " Restore the saved search and cursor position
    let @/=_s
    call cursor(l, c)
endfunction

autocmd BufWritePre * if &modified | call UpdateCopyrightYear() | endif

autocmd BufEnter * EnableStripWhitespaceOnSave


augroup CAuto
   au!

   " Use :make to compile c, even without a makefile
   "au FileType c,cpp if glob('Makefile') == "" | let &mp="gcc -o %< %" | endif
   au Filetype c set efm=%f:%l:%c:%m       " Lint compatible

   au FileType c,cpp setlocal foldmethod=syntax
augroup END


" Open tag in horiz/vert split
map <C-\> :sp <CR>:exec("tag ".expand("<cword>"))<CR>
map <A-\> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" tagbar
nmap <F4> :TagbarToggle<cr>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" airline
"let g:airline_theme='wombat'
"let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#branch#enabled = 1
"let g:airline#extensions#syntastic#enabled = 1
"let g:airline#extensions#tagbar#enabled = 1
"let g:airline#extensions#hunk#enabled = 1
"let g:airline#extensions#virtualenv#enabled = 1
"let g:airline#extensions#whitespace#enabled = 1
let g:airline#extensions#ale#enabled = 1

"if has("gui_running")
"    let g:airline_powerline_fonts = 1
"else
"    let g:airline_powerline_fonts = 0
"    let g:airline_symbols = {}
"    let g:airline_symbols.linenr = '␊'
"    let g:airline_symbols.linenr = '␤'
"    let g:airline_symbols.linenr = '¶'
"    let g:airline_symbols.branch = '⎇'
"    let g:airline_symbols.paste = 'ρ'
"    let g:airline_symbols.paste = 'Þ'
"    let g:airline_symbols.paste = '∥'
"    let g:airline_symbols.whitespace = 'Ξ'
"endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Fugitive
nnoremap <leader>ga :Git add %:p<cr><cr>
nnoremap <leader>gs :Gstatus<cr>
nnoremap <leader>gc :Gcommit -v -q<CR>
nnoremap <leader>gt :Gcommit -v -q %:p<CR>
nnoremap <leader>gd :Gdiff<CR>
nnoremap <leader>ge :Gedit<CR>
nnoremap <leader>gr :Gread<CR>
nnoremap <leader>gw :Gwrite<CR><CR>
nnoremap <leader>gl :silent! Glog<CR>:bot copen<CR>
nnoremap <leader>gp :Ggrep<Space>
nnoremap <leader>gm :Gmove<Space>
nnoremap <leader>gb :Git branch<Space>
nnoremap <leader>go :Git checkout<Space>
nnoremap <leader>gps :Dispatch! git push<CR>
nnoremap <leader>gpl :Dispatch! git pull<CR>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Erlang
let g:erlangCompleteFile = '~/vim/plugged/vimerl/autoload/erlang_complete.erl'


" altr
nmap <leader>a <Plug>(altr-forward)
nmap <leader>A <Plug>(altr-back)

