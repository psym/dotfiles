# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi

    [ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion
fi

# set PATH so it includes user's private bin if it exists
[ -d ~/bin ] && export PATH="~/bin:$PATH"

export R="https://hqswsvn/svn/Common_Basic_Services/AFS64XX/"
export RBS="https://hqswsvn/svn/Common_Basic_Services/AFS64XX/"
export RCS="https://hqswsvn/svn/Com_Server/"

export EDITOR=`which vi`
[ -x $(which vim)  ] && export EDITOR=`which vim`
[ -x $(which nvim) ] && export EDITOR=`which nvim`

alias vi=$EDITOR
alias vim=$EDITOR
alias nvim=$EDITOR

export HOMEBREW_CASK_OPTS="--appdir=~/Applications --fontdir=~/Library/Fonts"
