" This is the .vimrc equivalent!
filetype plugin indent on
syntax on

if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vimrc
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

let g:mapleader=','
let g:maplocalleader=','

nmap <silent> <leader>ev :e  $MYVIMRC<cr>
nmap <silent> <leader>sv :so $MYVIMRC<cr>
au! BufWritePost $MYVIMRC source $MYVIMRC


function! Install_LSPs(info)
   " info is a dictionary with 3 fields
   " - name:   name of the plugin
   " - status: 'installed', 'updated', or 'unchanged'
   " - force:  set on PlugInstall! or PlugUpdate!
   if a:info.status == 'installed' || a:info.status == 'updated' || a:info.force

      " LanguageClient-neovim
      let g:install_client = 'bash install.sh'

      " cquery
      let g:build_cquery = '('
         \ . ' (git clone https://github.com/cquery-project/cquery.git --recursive || (cd cquery && git pull))'
         \ . ' && cd cquery/cmake'
         \ . ' && cmake .. -DCMAKE_BUILD_TYPE=release '
         \ .              '-DCMAKE_INSTALL_PREFIX=release '
         \ .              '-DCMAKE_EXPORT_COMPILE_COMMANDS=YES '
         \ . ' && cmake --build . --target install'
         \ . ')'

      " ccls
      let g:build_ccls = '('
         \ . ' git clone https://github.com/MaskRay/ccls --depth=1'
         \ . ' && cd ccls'
         \ . ' && git submodule update --init'
         \ . ' && cmake -H. -BRelease -DCMAKE_EXPORT_COMPILE_COMMANDS=YES '
         \ .               '-DSYSTEM_CLANG=OFF'
         \ . ' && cmake --build Release'
         \ . ')'

      " Python Language Server
      let g:install_pls = "pip install --user 'python-language-server[all]'"

      execute '!' . g:install_client . '&& (' . g:build_cquery . ';' . g:install_pls . ')'
   endif
endfunction


call plug#begin('~/.config/nvim/plugged')

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
let g:deoplete#enable_at_startup = 1

"Plug 'Shougo/neosnippet.vim'
"Plug 'Shougo/neosnippet-snippets'
"imap <C-k>     <Plug>(neosnippet_expand_or_jump)
"smap <C-k>     <Plug>(neosnippet_expand_or_jump)
"xmap <C-k>     <Plug>(neosnippet_expand_target)
"smap <expr> <tab> neosnippet#expandable_or_jumpable() ?"\<Plug>(neosnippet_expand_or_jump)" : \<TAB>"
"inoremap <silent><expr><CR> pumvisible() ? deoplete#mappings#close_popup()."\<Plug>(neosnippet_expand_or_jump)" : "\<CR>"


if has('conceal')
   set conceallevel=2 concealcursor=niv
endif

" Use <TAB> to select the popup menu:
inoremap <expr> <tab>   pumvisible() ? "\<C-n>" : "\<tab>"
inoremap <expr> <S-tab> pumvisible() ? "\<C-p>" : "\<S-tab>"

inoremap <expr> <ESC>  pumvisible() ? "\<C-e>" : "\<Esc>"
inoremap <expr> <CR>   pumvisible() ? "\<C-y>" : "\<CR>"

" allow arrow keys when code completion window is up
inoremap <expr> <Down> pumvisible() ? "\<C-n>" : "\<Down>"
inoremap <expr> <Up>   pumvisible() ? "\<C-p>" : "\<Up>"

"inoremap <expr> <PageDown> pumvisible() ? "\<PageDown>\<C-p>\<C-n>" : "\<PageDown>"
"inoremap <expr> <PageUp> pumvisible() ? "\<PageUp>\<C-p>\<C-n>" : "\<PageUp>"
"inoremap <expr> <C-d> pumvisible() ? "\<PageDown>\<C-p>\<C-n>" : "\<C-d>"
"inoremap <expr> <C-u> pumvisible() ? "\<PageUp>\<C-p>\<C-n>" : "\<C-u>"

" Show parameter doc.
"Plug 'Shougo/echodoc.vim'

Plug 'Shougo/neco-vim'


" Indenting
"set expandtab           " Use spaces instead of tabs for autoindent/tab key
"set tabstop=3           " tabs look like 3 spaces
"set shiftwidth=3        " use 3 spaces when indenting (i.e. << or >>)
"set softtabstop=3
Plug 'tpope/vim-sleuth'

Plug 'autozimu/LanguageClient-neovim', {
	\ 'branch': 'next',
	\ 'do': function('Install_LSPs'),
	\ }
let g:LanguageClient_loggingFile = '/tmp/LanguageClient.log'
let g:LanguageClient_loggingLevel = 'INFO'
let g:LanguageClient_serverStderr = '/tmp/LanguageServer.log'

let g:LanguageClient_directory = '~/.config/nvim/plugged/LanguageClient-neovim/'
let g:cquery_path = g:LanguageClient_directory . '/cquery/cmake/release/bin/cquery'
let g:LanguageClient_serverCommands = {
	\ 'c': [g:cquery_path, '--log-file=/tmp/cq.log', '--init={"cacheDirectory":"/tmp/cquery/"}'],
	\ 'cpp': [g:cquery_path, '--log-file=/tmp/cq.log', '--init={"cacheDirectory":"/tmp/cquery/"}'],
        \ 'python': ['~/.local/bin/pyls']
	\ }
set completefunc=LanguageClient#complete
"set formatexpr=LanguageClient#textDocument_rangeFormatting_sync()

nnoremap <silent> gh :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> gf :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> gr :call LanguageClient#textDocument_references()<CR>
nnoremap <silent> gs :call LanguageClient#textDocument_documentSymbol()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>
nnoremap <silent> <F5> :call LanguageClient_contextMenu()<CR>

" Error: '✖', Warning: '⚠', Info: 'ℹ', Hint: '➤'
let g:LanguageClient_diagnosticDisplay = {
	\  1: {
   \	    "name": "Error",
   \      "texthl": "ALEError",
   \      "signText": "✖",
   \      "signTexthl": "ALEErrorSign",
   \     },
   \  2: {
   \      "name": "Warning",
   \      "texthl": "ALEWarning",
   \      "signText": "∆",
   \      "signTexthl": "ALEWarningSign",
   \     },
   \  3: {
   \      "name": "Information",
   \      "texthl": "ALEInfo",
   \      "signText": "➤",
   \      "signTexthl": "ALEInfoSign",
   \     },
   \  4: {
   \      "name": "Hint",
   \      "texthl": "ALEInfo",
   \      "signText": "➤",
   \      "signTexthl": "ALEInfoSign",
   \     },
   \ }


Plug 'junegunn/fzf'

Plug 'w0rp/ale'
let g:ale_linters = {
   \ 'c': ['clangtidy', 'cppcheck'],
   \ 'cpp': ['clangtidy', 'cppcheck'],
   \ 'zsh': ['shell'],
   \ 'bash': ['shell'],
   \ 'vim': ['vint'],
   \ 'text': ['proselint'],
   \ }
let g:ale_linters_aliases = {
   \ 'zsh': 'sh',
   \ 'bash': 'sh',
   \ }
if has('mac')
   "clang-tidy brew installed path on mac
   let g:ale_c_clangtidy_executable = '/usr/local/opt/llvm/bin/clang-tidy'
else
   let g:ale_c_clangtidy_executable = 'clang-tidy-7'
   let g:ale_c_clangformat_executable = 'clang-format-7'
   "let g:ale_c_clangformat_executable = g:cquery_path . '/cquery-clang-format'
endif

let g:ale_fixers = {
         \   'c': ['clang-format'],
         \   'cpp': ['clang-format'],
         \}
let g:ale_sign_column_always = 1
let g:ale_vim_vint_show_style_issues = 1

Plug 'kana/vim-altr'

Plug 'majutsushi/tagbar'
Plug 'ludovicchabant/vim-gutentags'
let g:gutentags_ctags_extra_args = ['--recurse', '--python-kinds=-i', '--c-kinds=+pl', '--c++-kinds=+pl', '--fields=+iaS', '--extra=+q']
set statusline+=%{gutentags#statusline()}
set tags=./tags;/


Plug 'justinmk/vim-dirvish'     "netwr is trash
Plug 'tpope/vim-eunuch'

Plug 'ntpeters/vim-better-whitespace'

Plug 'Lokaltog/vim-easymotion'

Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'ctrlpvim/ctrlp.vim'

"Plug 'altercation/vim-colors-solarized'
Plug 'ciaranm/inkpot'
Plug 'sheerun/vim-wombat-scheme'    " needs xterm-256color/screen-256color

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_theme='base16'

"Plug 'itchyny/lightline.vim'

call plug#end()

" lightline look and linter stuff
let g:lightline={
  \ 'active': {
  \   'left': [['mode','paste'],
  \             ['filename', 'modified', 'charvaluehex']],
  \   'right': [['lineinfo'], ['readonly', 'linter_warnings', 'linter_errors', 'linter_ok']]
  \ },
  \ 'component': {'charvaluehex': '0x%B'},
  \ 'component_expand': {
  \    'linter_warnings': 'LightlineLinterWarnings',
  \    'linter_errors': 'LightlineLinterErrors',
  \    'linter_ok': 'LightlineLinterOK'
  \ },
  \ 'component_type': {
  \    'readonly': 'error',
  \    'linter_warnings': 'warning',
  \    'linter_error': 'error'
  \ },
  \ }

function! LightlineLinterWarnings() abort
    let l:counts = ale#statusline#Count(bufnr(''))
    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:count.total - l:all_errors
    return l:counts.total == 0 ? '': printf('%d ', all_non_errors)
endfunction
function! LightlineLinterErrors() abort
    let l:counts = ale#statusline#Count(bufnr(''))
    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:counts.total - l:all_errors
    return l:counts.total == 0 ? '' : printf('%d ', all_errors)
endfunction
function! LightlineLinterOK() abort
        let l:counts = ale#statusline#Count(bufnr(''))
    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:counts.total - l:all_errors
    return l:counts.total == 0 ? '' : ''
endfunction

" use lightline but not in goyo
autocmd User ALELint call s:MaybeUpdateLightline()
function! s:MaybeUpdateLightline()
    if exists('#lightline')
        call lightline#update()
    end
 endfunction


"call deoplete#custom#source('LanguageClient', 'min_pattern_length', 1)

set colorcolumn=80
set hidden
set background=dark
set signcolumn=yes

colorscheme elflord
"colorscheme wombat
"
set shortmess+=a                        " Use [+] [RO] [w] for modified, read-only, modified
set showcmd                             " Display what command is waiting for an operator
set ruler                               " Show pos below the win if there's no status line
set laststatus=2                        " Always show statusline, even if only 1 window
set report=0                            " Notify me whenever any lines have changed
set confirm                             " Y-N-C prompt if closing with unsaved changes
set vb t_vb=                            " Disable visual bell

set listchars=tab:→\ ,eol:$,trail:•,precedes:<,extends:>

set number                              " Show line numbering
set numberwidth=1                       " Use 1 col + 1 space for numbers

set nobackup nowritebackup noswapfile
set noautowrite noautowriteall noautoread
set ffs=unix,dos,mac
set hidden                              " Allow unchanged buffers to hide
set scrolloff=3                         " Keep 3 lines below and above the cursor
set backspace=indent,eol,start          " Backspace over anything! (Super backspace!)
set showmatch                           " Briefly jump to the previous matching paren
set matchtime=2                         " For .2 seconds
set textwidth=80
set autoread                            " automatically re-read changes

set formatoptions=croqnj   " see :help fo-table

""" Searching
set ignorecase                          " Default to using case insensitive searches
set smartcase                           " unless uppercase is used in regex.
" Clear highlights
nmap <silent> <leader>/ :nohlsearch<cr>

set mouse=a             " Note: Hold Shift to copy/paste from external
set noshowmode


if (exists('+colorcolumn'))
   set colorcolumn=80
   highlight ColorColumn ctermbg=9
endif


set foldenable
set foldmethod=indent
set foldlevel=99
set foldlevelstart=99
set foldtext=FoldText()


" Improved Vim fold-text
" See: http://gregsexton.org/2011/03/27/improving-the-text-displayed-in-a-vim-fold.html
function! FoldText()
   " Get first non-blank line
   let l:fs = v:foldstart
   while getline(l:fs) =~? '^\s*$' | let l:fs = nextnonblank(l:fs + 1)
   endwhile
   if l:fs > v:foldend
      let l:line = getline(v:foldstart)
   else
      let l:line = substitute(getline(l:fs), '\t', repeat(' ', &tabstop), 'g')
   endif

   let l:w = winwidth(0) - &foldcolumn - (&number ? 8 : 0)
   let l:foldSize = 1 + v:foldend - v:foldstart
   let l:foldSizeStr = ' ' . l:foldSize . ' lines '
   let l:foldLevelStr = repeat('+--', v:foldlevel)
   let l:lineCount = line('$')
   let l:foldPercentage = printf('[%.1f', (l:foldSize*1.0)/l:lineCount*100) . '%] '
   let l:expansionString = repeat('.', l:w - strwidth(l:foldSizeStr.line.foldLevelStr.foldPercentage))
   return l:line . l:expansionString . l:foldSizeStr . l:foldPercentage . l:foldLevelStr
endfunction


"""" Command Line
set wildmode=list:longest
set wildignore+=*.o,*.obj,*.bak,*.exe,*.pyc,*.swp   " we don't want to edit these type of files

set completeopt=noinsert,menuone,noselect,longest

" allow command line editing like emacs
cnoremap <C-A>      <Home>
cnoremap <C-B>      <Left>
cnoremap <C-E>      <End>
cnoremap <C-F>      <Right>
cnoremap <C-N>      <End>
cnoremap <C-P>      <Up>
cnoremap <ESC>b     <S-Left>
cnoremap <ESC><C-B> <S-Left>
cnoremap <ESC>f     <S-Right>
cnoremap <ESC><C-F> <S-Right>
cnoremap <ESC><C-H> <C-W>

"""" Key Mappings
" work more logically with wrapped lines
noremap j gj
noremap k gk
nmap <leader>w :set invwrap<cr>

nnoremap ; :
imap jj <esc>
nmap <leader>p :set invpaste paste?<cr>
nmap <leader>rr :1,$retab<rc>

cmap w!! %!sudo tee > /dev/null %

" bind ctrl+space for omnicompletion
inoremap <Nul> <C-x><C-o>
imap <c-space> <c-x><c-o>

" Easy align interactive
vnoremap <silent> <Enter> :EasyAlign<cr>

" * to search the word in all files in dir
"nmap * :Ag <c-r>=expand("<cword>")<cr><cr>
" seach in files
"nnoremap <space>/ :Ag

""" Open/Close/Clear quickfix window
nmap <leader>c :copen<CR>
nmap <leader>cc :cclose<CR>
nmap <leader>cq :call setqflist([])<CR>

" Toggle spell checking
map <leader>ss :setlocal spell!<cr>

nnoremap <leader>l :call ToggleRelativeAbsoluteNumber()<CR>
" Vertical split with synchronized scrolling
nmap <leader>sb :call SplitScroll()<CR>

" Ctrl + Arrows - Move around quickly
nnoremap <c-up>     {
nnoremap <c-down>   }
nnoremap <c-right>  El
nnoremap <c-left>   Bh

" Shift + Arrows - Visual select
nnoremap <s-up>     Vk
nnoremap <s-down>   Vj
nnoremap <s-right>  vl
nnoremap <s-left>   vh

if &diff
   " easily handle diffing
   vnoremap < :diffget<CR>
   vnoremap > :diffput<CR>
else
   " visual shifting (builtin-repeat)
   vnoremap < <gv
   vnoremap > >gv
endif

" <space> toggles folds opened and closed
nnoremap <space> za
" <space> in visual mode creates a fold over the marked range
vnoremap <space> zf

" Delete Empty Lines
map <leader>del :g/^\s*$/d<CR>
" Delete Double Quoted Lines
map <leader>ddql :%s/^>\s*>.*//g<CR>
" Delete Dot Runs
map <leader>ddr :s/\.\+\s*/. /g<CR>
" Delete Space Runs
map <leader>dsr :s/\s\s\+/ /g<CR>

" Reindent everything
noremap <leader>= gg=G
" Reformat paragraph
noremap <leader>gp gqap
" Reformat everything
noremap <leader>gq gggqG
" Select everything
noremap <leader>gg ggVG

" Allow setting window title for screen
if &term =~ '^screen'
   set t_ts=k
   set t_fs=\
endif

" Vertical split current file with syncronized scrolling
function! SplitScroll()
   :wincmd v
   :wincmd w
   execute "normal! \<C-d>"
   :set scrollbind
   :wincmd w
   :set scrollbind
endfunction

function! ToggleRelativeAbsoluteNumber()
   if &number
      set relativenumber
   else
      set norelativenumber
   endif
endfunction



function! UpdateCopyrightYear()
    " Save the current search and cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")

   "http://vim.wikia.com/wiki/Automatically_Update_Copyright_Notice_in_Files
   exe '%s:'.
            \       '\cCOPYRIGHT\s*\%((c)\|©\|&copy;\)\?\s*'.
            \         '\%([0-9]\{4}\(-[0-9]\{4\}\)\?,\s*\)*\zs'.
            \         '\('.
            \           '\%('.strftime("%Y").'\)\@![0-9]\{4\}'.
            \           '\%(-'.strftime("%Y").'\)\@!\%(-[0-9]\{4\}\)\?'.
            \         '\&'.
            \           '\%([0-9]\{4\}-\)\?'.
            \           '\%('.(strftime("%Y")-1).'\)\@!'.
            \           '\%([0-9]\)\{4\}'.
            \         '\)'.
            \         '\ze\%(\%([0-9]\{4\}\)\@!.\)*$:'.
            \       '&, '.strftime("%Y").':e' |
            \ exe '%s:'.
            \       '\cCOPYRIGHT\s*\%((c)\|©\|&copy;\)\?\s*'.
            \         '\%([0-9]\{4}\%(-[0-9]\{4\}\)\?,\s*\)*\zs'.
            \           '\%('.strftime("%Y").'\)\@!\([0-9]\{4\}\)'.
            \           '\%(-'.strftime("%Y").'\)\@!\%(-[0-9]\{4\}\)\?'.
            \         '\ze\%(\%([0-9]\{4\}\)\@!.\)*$:'.
            \       '\1, '.strftime("%Y").':e'

    " Restore the saved search and cursor position
    let @/=_s
    call cursor(l, c)
endfunction

autocmd BufWritePre * if &modified | call UpdateCopyrightYear() | endif

autocmd BufEnter * EnableStripWhitespaceOnSave


augroup CAuto
   au!

   " Use :make to compile c, even without a makefile
   "au FileType c,cpp if glob('Makefile') == "" | let &mp="gcc -o %< %" | endif
   au Filetype c set efm=%f:%l:%c:%m       " Lint compatible

   au FileType c,cpp setlocal foldmethod=syntax
augroup END


" Open tag in horiz/vert split
map <C-\> :sp <CR>:exec("tag ".expand("<cword>"))<CR>
map <A-\> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" tagbar
nmap <F4> :TagbarToggle<cr>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" airline
"let g:airline_theme='wombat'
"let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#branch#enabled = 1
"let g:airline#extensions#syntastic#enabled = 1
"let g:airline#extensions#tagbar#enabled = 1
"let g:airline#extensions#hunk#enabled = 1
"let g:airline#extensions#virtualenv#enabled = 1
"let g:airline#extensions#whitespace#enabled = 1
let g:airline#extensions#ale#enabled = 1

"if has("gui_running")
"    let g:airline_powerline_fonts = 1
"else
"    let g:airline_powerline_fonts = 0
"    let g:airline_symbols = {}
"    let g:airline_symbols.linenr = '␊'
"    let g:airline_symbols.linenr = '␤'
"    let g:airline_symbols.linenr = '¶'
"    let g:airline_symbols.branch = '⎇'
"    let g:airline_symbols.paste = 'ρ'
"    let g:airline_symbols.paste = 'Þ'
"    let g:airline_symbols.paste = '∥'
"    let g:airline_symbols.whitespace = 'Ξ'
"endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Fugitive
nnoremap <leader>ga :Git add %:p<cr><cr>
nnoremap <leader>gs :Gstatus<cr>
nnoremap <leader>gc :Gcommit -v -q<CR>
nnoremap <leader>gt :Gcommit -v -q %:p<CR>
nnoremap <leader>gd :Gdiff<CR>
nnoremap <leader>ge :Gedit<CR>
nnoremap <leader>gr :Gread<CR>
nnoremap <leader>gw :Gwrite<CR><CR>
nnoremap <leader>gl :silent! Glog<CR>:bot copen<CR>
nnoremap <leader>gp :Ggrep<Space>
nnoremap <leader>gm :Gmove<Space>
nnoremap <leader>gb :Git branch<Space>
nnoremap <leader>go :Git checkout<Space>
nnoremap <leader>gps :Dispatch! git push<CR>
nnoremap <leader>gpl :Dispatch! git pull<CR>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Erlang
let g:erlangCompleteFile = '~/vim/plugged/vimerl/autoload/erlang_complete.erl'


" altr
nmap <leader>a <Plug>(altr-forward)
nmap <leader>A <Plug>(altr-back)

